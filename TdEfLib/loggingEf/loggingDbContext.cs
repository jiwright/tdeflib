namespace TdEfLib.loggingEf {
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class loggingDbContext : DbContext {
        public loggingDbContext(string connection_string) : base(connection_string) {
        }

        public virtual DbSet<tblCustomerDbLookup> tblCustomerDbLookups { get; set; }
        public virtual DbSet<tblDefinition> tblDefinitions { get; set; }
        public virtual DbSet<tblJobDbLookup> tblJobDbLookups { get; set; }
        public virtual DbSet<tblLog> tblLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<tblCustomerDbLookup>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomerDbLookup>()
                .Property(e => e.server)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomerDbLookup>()
                .Property(e => e.databaseName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.tableName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.columnName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.JobNumber)
                .HasPrecision(10, 2);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.server)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.databaseName)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.archivePath)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.grp1path)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.ejjPath)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.archiveName)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.webSiteVersion)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.runStatusFlag)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.userName)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.archiveMessage)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobDbLookup>()
                .Property(e => e.auditFindings)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.jobNumber)
                .HasPrecision(10, 1);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.logTypeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.moduleKey)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.parentId)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.moduleId)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.applicationId)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.logDetails)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.resolvedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.TempsubLogTypeID)
                .IsUnicode(false);

            modelBuilder.Entity<tblLog>()
                .Property(e => e.subLogTypeID)
                .IsUnicode(false);
        }
    }
}
