namespace TdEfLib.loggingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblCustomerDbLookup")]
    public partial class tblCustomerDbLookup
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string customerNumber { get; set; }

        [StringLength(50)]
        public string server { get; set; }

        [StringLength(50)]
        public string databaseName { get; set; }

        public int? databaseUse { get; set; }
    }
}
