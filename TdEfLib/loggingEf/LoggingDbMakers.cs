﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdEfLib.loggingEf {
    public class LoggingDbMakers {
        public static void logArchiveEntry(string logging_conn_string, int facility_id, string customer_number, int job_number, string log_details, string log_xml, int the_ranking, String created_by, string sub_log_type_id, int the_run_number) {
            using (loggingDbContext ldc = new loggingDbContext(logging_conn_string)) {
                tblLog tbl_log = new tblLog() {
                    facilityId = facility_id,
                    severityLevel = 0,
                    customerNumber = customer_number,
                    jobNumber = job_number,
                    logTypeId = "listJobArchive",
                    moduleId = "archive",
                    logDetails = log_details,
                    logXml = log_xml,
                    ranking = the_ranking,
                    eventDate = DateTime.Now,
                    created = DateTime.Now,
                    createdBy = created_by,
                    runDate = DateTime.Now,
                    subLogTypeID = sub_log_type_id,
                    runNumber = the_run_number
                };
                ldc.tblLogs.Add(tbl_log);
                ldc.SaveChanges();

            }
        }
        public static List<tblLog> getLogEntries(string logging_conn_string, int facility_id, string customer_number, int job_number, string log_details, string log_xml, int the_ranking, String created_by, string sub_log_type_id, int the_run_number) {
            List<tblLog> r_list = null;
            using (loggingDbContext ldc = new loggingDbContext(logging_conn_string)) {
                r_list = ldc.tblLogs.Where(q => q.jobNumber == job_number && q.facilityId == facility_id).ToList();
            }
            return r_list;
        }
    }
}