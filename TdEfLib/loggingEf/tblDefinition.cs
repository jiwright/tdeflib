namespace TdEfLib.loggingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblDefinition
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string tableName { get; set; }

        [StringLength(30)]
        public string columnName { get; set; }

        public int? fieldValue { get; set; }

        [StringLength(50)]
        public string description { get; set; }
    }
}
