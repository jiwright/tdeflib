namespace TdEfLib.loggingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblJobDbLookup")]
    public partial class tblJobDbLookup
    {
        [Key]
        public int dbId { get; set; }

        [StringLength(30)]
        public string customerNumber { get; set; }

        public decimal? JobNumber { get; set; }

        public int? jobTypeId { get; set; }

        [StringLength(50)]
        public string server { get; set; }

        [StringLength(50)]
        public string databaseName { get; set; }

        public int? databaseUse { get; set; }

        public int archived { get; set; }

        [StringLength(1000)]
        public string archivePath { get; set; }

        [StringLength(1000)]
        public string grp1path { get; set; }

        [StringLength(1000)]
        public string ejjPath { get; set; }

        [StringLength(1000)]
        public string archiveName { get; set; }

        public DateTime? archiveDate { get; set; }

        public int? jobPid { get; set; }

        public DateTime? created { get; set; }

        [StringLength(10)]
        public string webSiteVersion { get; set; }

        public int? facilityId { get; set; }

        [StringLength(100)]
        public string runStatusFlag { get; set; }

        [StringLength(100)]
        public string userName { get; set; }

        public DateTime? modified { get; set; }

        public int? dbCount { get; set; }

        public int? sourceCount { get; set; }

        public string archiveMessage { get; set; }

        public DateTime? lastAudited { get; set; }

        [StringLength(200)]
        public string auditFindings { get; set; }
    }
}
