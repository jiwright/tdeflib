namespace TdEfLib.loggingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblLog
    {
        [Key]
        public int eventId { get; set; }

        [StringLength(30)]
        public string customerNumber { get; set; }

        public decimal? jobNumber { get; set; }

        public int? jobTypeId { get; set; }

        [StringLength(30)]
        public string logTypeId { get; set; }

        public double? activityCode { get; set; }

        [StringLength(50)]
        public string moduleKey { get; set; }

        public int? severityLevel { get; set; }

        [StringLength(25)]
        public string parentId { get; set; }

        [StringLength(50)]
        public string moduleId { get; set; }

        [StringLength(50)]
        public string applicationId { get; set; }

        [Column(TypeName = "xml")]
        public string logXml { get; set; }

        [StringLength(200)]
        public string logDetails { get; set; }

        public int? ranking { get; set; }

        public DateTime? eventDate { get; set; }

        [Column(TypeName = "xml")]
        public string responseXml { get; set; }

        public bool? resolveRequired { get; set; }

        public DateTime? resolveDate { get; set; }

        [StringLength(20)]
        public string resolvedBy { get; set; }

        public DateTime? created { get; set; }

        [StringLength(50)]
        public string createdBy { get; set; }

        public bool? isCurrent { get; set; }

        public DateTime? runDate { get; set; }

        [StringLength(20)]
        public string TempsubLogTypeID { get; set; }

        [StringLength(30)]
        public string subLogTypeID { get; set; }

        public int? facilityId { get; set; }

        public int? runNumber { get; set; }
    }
}
