namespace TdEfLib.scheduleEF {
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class scheduleDbContext : DbContext {
        public scheduleDbContext(string connection_string) : base(connection_string) {
        }

        public virtual DbSet<tblDate> tblDates { get; set; }
        public virtual DbSet<tblDefinition> tblDefinitions { get; set; }
        public virtual DbSet<tblJobStatusRollupNational> tblJobStatusRollupNationals { get; set; }
        public virtual DbSet<tblJobTicketAction> tblJobTicketActions { get; set; }
        public virtual DbSet<tblPrintStreamMonitor> tblPrintStreamMonitors { get; set; }
        public virtual DbSet<tblPSMasterQuote> tblPSMasterQuotes { get; set; }
        public virtual DbSet<tblCustomer> tblCustomers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<tblDate>()
                .Property(e => e.jobNumber)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.dateTypeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.dateTypeDetail)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.assignedTo)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.scheduledBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.completedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.programId)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.notificationTo)
                .IsUnicode(false);

            modelBuilder.Entity<tblDate>()
                .Property(e => e.notificationDescription)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.tableName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.columnName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.fieldValueType)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.category)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.jobNumber)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.JobName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.CsrRecnum)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.SalesrepRecnum)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.AcctMgr)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.SAE)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.Client)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.ClientNumber)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.InvoicedAmount)
                .HasPrecision(10, 2);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.ActivityRollup)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .Property(e => e.clientProject)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobStatusRollupNational>()
                .HasMany(e => e.tblDates)
                .WithOptional(e => e.tblJobStatusRollupNational)
                .HasForeignKey(e => new { e.jobNumber, e.facilityId });

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.jobName)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.userId)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.jobTicketAction)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.psFlagJobNumber)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.stageOrProduction)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.jobTicketActionType)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.cadence)
                .IsUnicode(false);

            modelBuilder.Entity<tblJobTicketAction>()
                .Property(e => e.quartz)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrintStreamMonitor>()
                .Property(e => e.psUser)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrintStreamMonitor>()
                .Property(e => e.system)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrintStreamMonitor>()
                .Property(e => e.message)
                .IsUnicode(false);

            modelBuilder.Entity<tblPrintStreamMonitor>()
                .Property(e => e.error)
                .IsUnicode(false);

            modelBuilder.Entity<tblPSMasterQuote>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblPSMasterQuote>()
                .Property(e => e.packageXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.pk)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.earlyPct)
                .HasPrecision(5, 4);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.onTimePct)
                .HasPrecision(5, 4);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.latePct)
                .HasPrecision(5, 4);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.bccId)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.bccName)
                .IsUnicode(false);
        }
    }
}
