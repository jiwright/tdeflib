namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblDate
    {
        [Key]
        public int pk { get; set; }

        public decimal? jobNumber { get; set; }

        public int? tempJobNumber { get; set; }

        public int? facilityId { get; set; }

        public int? jobTypeId { get; set; }

        [Required]
        [StringLength(25)]
        public string dateTypeId { get; set; }

        [StringLength(50)]
        public string dateTypeDetail { get; set; }

        public DateTime? scheduledDate { get; set; }

        public DateTime? completedDate { get; set; }

        public DateTime created { get; set; }

        [StringLength(50)]
        public string createdBy { get; set; }

        [StringLength(50)]
        public string assignedTo { get; set; }

        [StringLength(50)]
        public string scheduledBy { get; set; }

        [StringLength(50)]
        public string completedBy { get; set; }

        [StringLength(50)]
        public string programId { get; set; }

        public bool isCurrent { get; set; }

        public bool? isVisible { get; set; }

        [StringLength(300)]
        public string notificationTo { get; set; }

        public int? notificationDays { get; set; }

        public DateTime? lastNotificationDate { get; set; }

        public int? notificationCount { get; set; }

        [StringLength(2000)]
        public string notificationDescription { get; set; }

        public virtual tblJobStatusRollupNational tblJobStatusRollupNational { get; set; }
    }
}
