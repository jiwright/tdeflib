namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblPSMasterQuote
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jobTypeId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string customerNumber { get; set; }

        [Required]
        public string packageXML { get; set; }

        [Column(TypeName = "numeric")]
        public decimal quoteNumber { get; set; }

        public DateTime created { get; set; }
    }
}
