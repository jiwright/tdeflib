namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblJobStatusRollupNational")]
    public partial class tblJobStatusRollupNational
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblJobStatusRollupNational()
        {
            tblDates = new HashSet<tblDate>();
        }

        [Key]
        [Column(Order = 0)]
        public decimal jobNumber { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int facilityId { get; set; }

        public int? tempJobNumber { get; set; }

        public int? jobTypeId { get; set; }

        [Column("QUOTE NO")]
        public double? QUOTE_NO { get; set; }

        [StringLength(49)]
        public string JobName { get; set; }

        public DateTime? JobDueDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CsrRecnum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SalesrepRecnum { get; set; }

        [StringLength(50)]
        public string AcctMgr { get; set; }

        [StringLength(50)]
        public string SAE { get; set; }

        [StringLength(40)]
        public string Client { get; set; }

        [StringLength(8)]
        public string ClientNumber { get; set; }

        public double? quantity { get; set; }

        public decimal? InvoicedAmount { get; set; }

        public int? InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        [StringLength(250)]
        public string ActivityRollup { get; set; }

        [StringLength(30)]
        public string clientProject { get; set; }

        public DateTime? created { get; set; }

        public int? keepAlive { get; set; }

        public DateTime? KeepAliveDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblDate> tblDates { get; set; }
    }
}
