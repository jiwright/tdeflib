namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblJobTicketAction
    {
        [StringLength(49)]
        public string jobName { get; set; }

        [StringLength(99)]
        public string userId { get; set; }

        [Key]
        public int tempJobNumber { get; set; }

        public int? jobTypeId { get; set; }

        public int? facilityId { get; set; }

        [StringLength(30)]
        public string customerNumber { get; set; }

        public DateTime? inHomeDate { get; set; }

        [StringLength(50)]
        public string jobTicketAction { get; set; }

        public DateTime? actionEntered { get; set; }

        public DateTime? actionFinished { get; set; }

        public decimal? psFlagJobNumber { get; set; }

        public int? timesSuccessful { get; set; }

        public int? timesFailed { get; set; }

        public DateTime? created { get; set; }

        [StringLength(99)]
        public string stageOrProduction { get; set; }

        public int? timesPostedToProduction { get; set; }

        [StringLength(50)]
        public string jobTicketActionType { get; set; }

        public int? isCurrent { get; set; }

        [StringLength(50)]
        public string cadence { get; set; }

        [StringLength(100)]
        public string quartz { get; set; }
    }
}
