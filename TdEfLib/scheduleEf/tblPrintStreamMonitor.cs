namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblPrintStreamMonitor")]
    public partial class tblPrintStreamMonitor
    {
        [Key]
        public int pk { get; set; }

        public int facilityId { get; set; }

        public DateTime monitorDateTime { get; set; }

        [Required]
        [StringLength(50)]
        public string psUser { get; set; }

        public int isLoggedIn { get; set; }

        public int? isXmlCreatorActive { get; set; }

        [StringLength(50)]
        public string system { get; set; }

        [StringLength(400)]
        public string message { get; set; }

        [StringLength(400)]
        public string error { get; set; }
    }
}
