namespace TdEfLib.scheduleEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblCustomer
    {
        public int? facilityId { get; set; }

        [StringLength(50)]
        public string customerNumber { get; set; }

        [StringLength(40)]
        public string customerName { get; set; }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal pk { get; set; }

        public bool? allowPlanetCodes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? earlyPct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? onTimePct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? latePct { get; set; }

        public DateTime? created { get; set; }

        [StringLength(20)]
        public string bccId { get; set; }

        [StringLength(100)]
        public string bccName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? bccExpirationdate { get; set; }
    }
}
