namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblBrandApprover
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string customerId { get; set; }

        [StringLength(200)]
        public string approverName { get; set; }

        [StringLength(500)]
        public string approverEmail { get; set; }

        [StringLength(100)]
        public string approverTitle { get; set; }

        [StringLength(20)]
        public string phone { get; set; }
    }
}
