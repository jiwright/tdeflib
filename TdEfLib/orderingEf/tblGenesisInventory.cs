using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace TdEfLib.orderingEf {
    [Table("tblGenesisInventory")]
    public partial class tblGenesisInventory {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblGenesisInventory()
        {
            tblGenesisOrderStores = new HashSet<tblGenesisOrderStore>();
        }

        [Key]
        public int pk { get; set; }

        public int? itemNumber { get; set; }

        public string itemName { get; set; }

        [StringLength(50)]
        public string genesisId { get; set; }

        [StringLength(50)]
        public string version { get; set; }

        public int? quantity { get; set; }

        public int? startingQuantity { get; set; }

        [JsonIgnore]
        public DateTime? created { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string createdBy { get; set; }

        [JsonIgnore]
        public DateTime? deleted { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string deletedby { get; set; }

        [JsonIgnore]
        public bool? isCurrent { get; set; }

        [JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblGenesisOrderStore> tblGenesisOrderStores { get; set; }
    }
}
