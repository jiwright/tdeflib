namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblBillingAllocation")]
    public partial class tblBillingAllocation
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string customerNumber { get; set; }

        public int? jobTypeId { get; set; }

        [StringLength(30)]
        public string activityCode { get; set; }

        [StringLength(50)]
        public string billingType { get; set; }

        public int? allocationType { get; set; }

        public decimal? allocationRate { get; set; }
    }
}
