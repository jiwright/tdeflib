namespace TdEfLib.orderingEf {
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class orderingDbContext : DbContext {
        public orderingDbContext(string connection_string) : base(connection_string) {
        }

        public virtual DbSet<tblBillingAllocation> tblBillingAllocations { get; set; }
        public virtual DbSet<tblBrandStoreProfile> tblBrandStoreProfiles { get; set; }
        public virtual DbSet<tblCustomer> tblCustomers { get; set; }
        public virtual DbSet<tblDefinition> tblDefinitions { get; set; }
        public virtual DbSet<tblFacility> tblFacilities { get; set; }
        public virtual DbSet<tblGenesisInventory> tblGenesisInventories { get; set; }
        public virtual DbSet<tblGenesisOrderStore> tblGenesisOrderStores { get; set; }
        public virtual DbSet<tblOrderApproval> tblOrderApprovals { get; set; }
        public virtual DbSet<tblOrderApprovalHistory> tblOrderApprovalHistories { get; set; }
        public virtual DbSet<tblOrderApprover> tblOrderApprovers { get; set; }
        public virtual DbSet<tblOrderFile> tblOrderFiles { get; set; }
        public virtual DbSet<tblOrderFileVersionApproval> tblOrderFileVersionApprovals { get; set; }
        public virtual DbSet<tblOrderFileVersionApprovalHistory> tblOrderFileVersionApprovalHistories { get; set; }
        public virtual DbSet<tblOrderFileVersion> tblOrderFileVersions { get; set; }
        public virtual DbSet<tblOrderJob> tblOrderJobs { get; set; }
        public virtual DbSet<tblOrder> tblOrders { get; set; }
        public virtual DbSet<tblOrderStoreAddressAllocation> tblOrderStoreAddressAllocations { get; set; }
        public virtual DbSet<tblOrderStoreApproval> tblOrderStoreApprovals { get; set; }
        public virtual DbSet<tblOrderStoreApprovalHistory> tblOrderStoreApprovalHistories { get; set; }
        public virtual DbSet<tblOrderStore> tblOrderStores { get; set; }
        public virtual DbSet<tblStoreProfileCustomerField> tblStoreProfileCustomerFields { get; set; }
        public virtual DbSet<tblStoreProfile> tblStoreProfiles { get; set; }
        public virtual DbSet<tblAuditStore> tblAuditStores { get; set; }
        public virtual DbSet<tblBrandApprover> tblBrandApprovers { get; set; }
        public virtual DbSet<tblBrandCustomer> tblBrandCustomers { get; set; }
        public virtual DbSet<tblMarketCoverageGeo> tblMarketCoverageGeos { get; set; }
        public virtual DbSet<tblPagePreference> tblPagePreferences { get; set; }
        public virtual DbSet<tblStoreMarketCoverage> tblStoreMarketCoverages { get; set; }
        public virtual DbSet<vwGenesisOrderShippingHistory> vwGenesisOrderShippingHistories { get; set; }
        public virtual DbSet<vwStoreProfilesWithOverlay> vwStoreProfilesWithOverlays { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblBillingAllocation>()
                .Property(e => e.customerNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillingAllocation>()
                .Property(e => e.activityCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillingAllocation>()
                .Property(e => e.billingType)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillingAllocation>()
                .Property(e => e.allocationRate)
                .HasPrecision(10, 4);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.parentId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.storeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.marketManagerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.marketManager)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.districtLeader)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.districtLeaderEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.address2)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.county)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.crrt)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.zip42r)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.areaCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.phoneType)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.areaCode2)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.phone2)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.phone2Type)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.faxareaCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.latitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.longitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.market)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.storeMapXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.webUrl)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.storeType)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.valueGroup)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.brandNameId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.brandName)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.bio)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.dpvReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.code1ReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.houseNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.leadingDir)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.streetName)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.streetSuffix)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.trailingDir)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.dpvFalsePositiveFlag)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.dpvCmraFlag)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.addressKey)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.scf)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandStoreProfile>()
                .Property(e => e.bmc)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerName)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.CustomerContact)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerCity)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerZip)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.updatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerIndustry)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.customerState)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.supportMail)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.suiteNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.webUrl)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.firstName)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.lastName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.tableName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.columnName)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.fieldValueType)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.category)
                .IsUnicode(false);

            modelBuilder.Entity<tblDefinition>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.facilityName)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.address1)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.address2)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblFacility>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .Property(e => e.itemName)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .Property(e => e.genesisId)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .Property(e => e.version)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .Property(e => e.deletedby)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisInventory>()
                .HasMany(e => e.tblGenesisOrderStores)
                .WithOptional(e => e.tblGenesisInventory)
                .HasForeignKey(e => e.genesisInventoryPk);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.attention)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.fedExLabelNote)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.trackingNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.shippingNotes)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.shippedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.createdby)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.modifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.deletedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblGenesisOrderStore>()
                .Property(e => e.submittedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApproval>()
                .Property(e => e.lastUserEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApproval>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApproval>()
                .Property(e => e.orderApprovalXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApproval>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprovalHistory>()
                .Property(e => e.userEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprovalHistory>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprovalHistory>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprover>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprover>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprover>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderApprover>()
                .Property(e => e.proofUserType)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFile>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFile>()
                .Property(e => e.versionCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApproval>()
                .Property(e => e.lastUpdatedEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApproval>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApproval>()
                .Property(e => e.orderApprovalXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApproval>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApproval>()
                .Property(e => e.approvalEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApprovalHistory>()
                .Property(e => e.userEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApprovalHistory>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApprovalHistory>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApprovalHistory>()
                .Property(e => e.approvalEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersionApprovalHistory>()
                .Property(e => e.approvalTitle)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.origFileName)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.internalFileName)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.fileSize)
                .HasPrecision(20, 4);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.uuid)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderFileVersion>()
                .Property(e => e.lastUpdatedEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderJob>()
                .Property(e => e.jobNumber)
                .HasPrecision(10, 0);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.campaignName)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.updatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.orderSetupXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblGenesisOrderStores)
                .WithOptional(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderApprovals)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderApprovalHistories)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderApprovers)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderFiles)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderFileVersionApprovals)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderJobs)
                .WithOptional(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderStores)
                .WithOptional(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderStoreApprovals)
                .WithRequired(e => e.tblOrder)
                .HasForeignKey(e => e.orderPk)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrderStoreAddressAllocation>()
                .Property(e => e.inHomeDateS)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreAddressAllocation>()
                .Property(e => e.crrt)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApproval>()
                .Property(e => e.lastUserEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApproval>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApproval>()
                .Property(e => e.orderApprovalXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApproval>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApprovalHistory>()
                .Property(e => e.userEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApprovalHistory>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStoreApprovalHistory>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStore>()
                .Property(e => e.orderStoreXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStore>()
                .Property(e => e.selectedPostcard)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStore>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderStore>()
                .Property(e => e.updatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfileCustomerField>()
                .Property(e => e.columnName)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfileCustomerField>()
                .Property(e => e.label)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfileCustomerField>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfileCustomerField>()
                .Property(e => e.sampleData)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfileCustomerField>()
                .Property(e => e.toString)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.parentId)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.storeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.marketManagerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.marketManager)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.districtLeader)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.districtLeaderEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.address2)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.county)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.crrt)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.zip42r)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.areaCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.phoneType)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.areaCode2)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.phone2)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.phone2Type)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.faxareaCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.latitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.longitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.market)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.storeMapXML)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.webUrl)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.storeType)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.valueGroup)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.brandNameId)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.brandName)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.bio)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.dpvReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.code1ReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.houseNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.leadingDir)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.streetName)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.streetSuffix)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.trailingDir)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.dpvFalsePositiveFlag)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.dpvCmraFlag)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.addresskey)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.scf)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .Property(e => e.bmc)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreProfile>()
                .HasMany(e => e.tblGenesisOrderStores)
                .WithOptional(e => e.tblStoreProfile)
                .HasForeignKey(e => e.storePk);

            modelBuilder.Entity<tblStoreProfile>()
                .HasMany(e => e.tblOrderStores)
                .WithOptional(e => e.tblStoreProfile)
                .HasForeignKey(e => e.storePk);

            modelBuilder.Entity<tblBrandApprover>()
                .Property(e => e.customerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandApprover>()
                .Property(e => e.approverName)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandApprover>()
                .Property(e => e.approverEmail)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandApprover>()
                .Property(e => e.approverTitle)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandApprover>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandCustomer>()
                .Property(e => e.brandCustomerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandCustomer>()
                .Property(e => e.brandName)
                .IsUnicode(false);

            modelBuilder.Entity<tblBrandCustomer>()
                .Property(e => e.franchiseCustomerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblMarketCoverageGeo>()
                .Property(e => e.baseCoverage)
                .IsUnicode(false);

            modelBuilder.Entity<tblMarketCoverageGeo>()
                .Property(e => e.geoValue)
                .IsUnicode(false);

            modelBuilder.Entity<tblPagePreference>()
                .Property(e => e.pageName)
                .IsUnicode(false);

            modelBuilder.Entity<tblPagePreference>()
                .Property(e => e.jsonPreferences)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.customerId)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.subCoverageGroup)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.CoverageLevel)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.percentCoverage)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.county)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.crrt)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.updatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.baseLevel)
                .IsUnicode(false);

            modelBuilder.Entity<tblStoreMarketCoverage>()
                .Property(e => e.geoXML)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.attn)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.fedExLabelNote)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.trackingId)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.fulfillmentNotes)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.shippedBy)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.orderItemBy)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.itemName)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.genesisId)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.version)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.locationId)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.locationName)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.address2)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.monthlyOrder)
                .IsUnicode(false);

            modelBuilder.Entity<vwGenesisOrderShippingHistory>()
                .Property(e => e.orderBy)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.parentId)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.storeId)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.marketManagerId)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.marketManager)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.districtLeader)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.districtLeaderEmail)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.address2)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.county)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.crrt)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.zip42r)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.areaCode)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.phoneType)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.areaCode2)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.phone2)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.phone2Type)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.faxareaCode)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.fax)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.createdBy)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.latitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.longitude)
                .HasPrecision(19, 12);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.market)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.storeMapXML)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.webUrl)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.storeType)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.valueGroup)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.brandNameId)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.brandName)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.bio)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.dpvReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.code1ReturnCode)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.houseNumber)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.leadingDir)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.streetName)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.streetSuffix)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.trailingDir)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.dpvFalsePositiveFlag)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.dpvCmraFlag)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.addresskey)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.scf)
                .IsUnicode(false);

            modelBuilder.Entity<vwStoreProfilesWithOverlay>()
                .Property(e => e.bmc)
                .IsUnicode(false);
        }
    }
}
