namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderStoreAddressAllocation")]
    public partial class tblOrderStoreAddressAllocation
    {
        [Key]
        public int pk { get; set; }

        public int orderPk { get; set; }

        public int storePk { get; set; }

        [StringLength(10)]
        public string inHomeDateS { get; set; }

        public int zip { get; set; }

        [Required]
        [StringLength(50)]
        public string crrt { get; set; }
    }
}
