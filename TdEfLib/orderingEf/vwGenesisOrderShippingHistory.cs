namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwGenesisOrderShippingHistory")]
    public partial class vwGenesisOrderShippingHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PrimaryKey { get; set; }

        public int? monthlyOrderID { get; set; }

        public int? locationPk { get; set; }

        public int? genesisInventoryPk { get; set; }

        public int? shipping { get; set; }

        [StringLength(50)]
        public string attn { get; set; }

        [StringLength(50)]
        public string fedExLabelNote { get; set; }

        public int? unitSize { get; set; }

        public int? units { get; set; }

        [StringLength(50)]
        public string trackingId { get; set; }

        [StringLength(150)]
        public string fulfillmentNotes { get; set; }

        public DateTime? shipDate { get; set; }

        [StringLength(256)]
        public string shippedBy { get; set; }

        public bool? orderItemShipStatus { get; set; }

        public DateTime? orderItemDate { get; set; }

        [StringLength(256)]
        public string orderItemBy { get; set; }

        public bool? orderStatus { get; set; }

        public int? itemId { get; set; }

        public string itemName { get; set; }

        [StringLength(50)]
        public string genesisId { get; set; }

        [StringLength(50)]
        public string version { get; set; }

        [StringLength(20)]
        public string locationId { get; set; }

        [StringLength(400)]
        public string locationName { get; set; }

        [StringLength(400)]
        public string address { get; set; }

        [StringLength(40)]
        public string address2 { get; set; }

        [StringLength(25)]
        public string city { get; set; }

        [StringLength(2)]
        public string state { get; set; }

        [StringLength(10)]
        public string zip { get; set; }

        public string monthlyOrder { get; set; }

        public DateTime? orderDate { get; set; }

        [StringLength(256)]
        public string orderBy { get; set; }

        public bool? currentItemStatus { get; set; }
    }
}
