namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblStoreMarketCoverage")]
    public partial class tblStoreMarketCoverage
    {
        [Key]
        [Column(Order = 0)]
        public int pk { get; set; }

        [StringLength(50)]
        public string customerId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int storePk { get; set; }

        [StringLength(10)]
        public string subCoverageGroup { get; set; }

        [StringLength(50)]
        public string CoverageLevel { get; set; }

        [StringLength(20)]
        public string percentCoverage { get; set; }

        [StringLength(500)]
        public string county { get; set; }

        [StringLength(10)]
        public string state { get; set; }

        [StringLength(10)]
        public string zip { get; set; }

        [StringLength(20)]
        public string crrt { get; set; }

        [StringLength(200)]
        public string createdBy { get; set; }

        public DateTime? created { get; set; }

        [StringLength(200)]
        public string updatedBy { get; set; }

        public DateTime? updated { get; set; }

        [StringLength(20)]
        public string baseLevel { get; set; }

        public string geoXML { get; set; }
    }
}
