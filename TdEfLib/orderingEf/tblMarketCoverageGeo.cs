namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblMarketCoverageGeo
    {
        public int? storeMarketCoveragePk { get; set; }

        [Key]
        public int pk { get; set; }

        [StringLength(20)]
        public string baseCoverage { get; set; }

        [StringLength(20)]
        public string geoValue { get; set; }
    }
}
