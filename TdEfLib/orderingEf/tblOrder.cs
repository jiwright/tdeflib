namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrder()
        {
            tblGenesisOrderStores = new HashSet<tblGenesisOrderStore>();
            tblOrderApprovals = new HashSet<tblOrderApproval>();
            tblOrderApprovalHistories = new HashSet<tblOrderApprovalHistory>();
            tblOrderApprovers = new HashSet<tblOrderApprover>();
            tblOrderFiles = new HashSet<tblOrderFile>();
            tblOrderFileVersionApprovals = new HashSet<tblOrderFileVersionApproval>();
            tblOrderJobs = new HashSet<tblOrderJob>();
            tblOrderStores = new HashSet<tblOrderStore>();
            tblOrderStoreApprovals = new HashSet<tblOrderStoreApproval>();
        }

        [Key]
        public int pk { get; set; }

        public int? customerId { get; set; }

        public string campaignName { get; set; }

        public DateTime? created { get; set; }

        [StringLength(256)]
        public string createdBy { get; set; }

        public DateTime? updatedOn { get; set; }

        [StringLength(256)]
        public string updatedBy { get; set; }

        public string orderSetupXML { get; set; }

        public DateTime? invoiceDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblGenesisOrderStore> tblGenesisOrderStores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderApproval> tblOrderApprovals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderApprovalHistory> tblOrderApprovalHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderApprover> tblOrderApprovers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderFile> tblOrderFiles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderFileVersionApproval> tblOrderFileVersionApprovals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderJob> tblOrderJobs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderStore> tblOrderStores { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderStoreApproval> tblOrderStoreApprovals { get; set; }
    }
}
