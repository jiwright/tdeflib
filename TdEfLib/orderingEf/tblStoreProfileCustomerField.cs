namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblStoreProfileCustomerField")]
    public partial class tblStoreProfileCustomerField
    {
        [Key]
        public int pk { get; set; }

        public int? customerNumber { get; set; }

        [StringLength(55)]
        public string columnName { get; set; }

        [StringLength(55)]
        public string label { get; set; }

        [StringLength(55)]
        public string source { get; set; }

        public bool? required { get; set; }

        [StringLength(2000)]
        public string sampleData { get; set; }

        [StringLength(55)]
        public string toString { get; set; }
    }
}
