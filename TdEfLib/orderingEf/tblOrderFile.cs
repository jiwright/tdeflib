namespace TdEfLib.orderingEf
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrderFile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrderFile()
        {
            tblOrderFileVersions = new HashSet<tblOrderFileVersion>();
        }

        public int orderPk { get; set; }

        [Key]
        public int filePk { get; set; }

        public int? fileTypeId { get; set; }

        public int? sourceType { get; set; }

        [JsonIgnore]
        public DateTime? createdDate { get; set; }

        [JsonIgnore]
        [StringLength(100)]
        public string createdBy { get; set; }

        [JsonIgnore]
        public bool? isCurrent { get; set; }

        [JsonIgnore]
        [StringLength(30)]
        public string versionCode { get; set; }
        [JsonIgnore]
        public virtual tblOrder tblOrder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<tblOrderFileVersion> tblOrderFileVersions { get; set; }
    }
}
