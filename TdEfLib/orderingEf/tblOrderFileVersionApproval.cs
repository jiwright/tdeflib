namespace TdEfLib.orderingEf {
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderFileVersionApproval")]
    public partial class tblOrderFileVersionApproval {
        [Key]
        public int pk { get; set; }

        [JsonIgnore]
        public int? customerId { get; set; }

        [JsonIgnore]
        public int orderPk { get; set; }

        [JsonIgnore]
        public int fileVersionPk { get; set; }

        [JsonIgnore]
        public int? proofTypeId { get; set; }

        [JsonIgnore]
        public int? approvalType { get; set; }

        public int? approvalStatus { get; set; }

        [JsonIgnore]
        public int? lastUpdateCount { get; set; }

        [JsonIgnore]
        [StringLength(100)]
        public string lastUpdatedEmail { get; set; }

        public DateTime? lastUpdateDate { get; set; }

        [JsonIgnore]
        public DateTime? created { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string createdBy { get; set; }

        [JsonIgnore]
        public string orderApprovalXML { get; set; }

        public string notes { get; set; }

        [StringLength(200)]
        public string approvalEmail { get; set; }

        public int? approvalLevel { get; set; }

        [JsonIgnore]
        public virtual tblOrderFileVersion tblOrderFileVersion { get; set; }

        [JsonIgnore]
        public virtual tblOrder tblOrder { get; set; }
    }
}
