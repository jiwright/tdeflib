namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblPagePreference
    {
        [Key]
        [Column(Order = 0)]
        public int pk { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string pageName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string jsonPreferences { get; set; }
    }
}
