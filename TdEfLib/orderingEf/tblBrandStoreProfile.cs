namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblBrandStoreProfile
    {
        [Key]
        public int pk { get; set; }

        public int? brandPk { get; set; }

        public int? storePk { get; set; }

        [StringLength(20)]
        public string parentId { get; set; }

        [StringLength(20)]
        public string storeId { get; set; }

        [StringLength(400)]
        public string location { get; set; }

        [StringLength(20)]
        public string marketManagerId { get; set; }

        [StringLength(55)]
        public string marketManager { get; set; }

        [StringLength(55)]
        public string districtLeader { get; set; }

        [StringLength(500)]
        public string districtLeaderEmail { get; set; }

        [StringLength(400)]
        public string address { get; set; }

        [StringLength(40)]
        public string address2 { get; set; }

        [StringLength(25)]
        public string city { get; set; }

        [StringLength(50)]
        public string county { get; set; }

        [StringLength(2)]
        public string state { get; set; }

        [StringLength(10)]
        public string zip { get; set; }

        [StringLength(5)]
        public string crrt { get; set; }

        [StringLength(10)]
        public string zip42r { get; set; }

        [StringLength(3)]
        public string areaCode { get; set; }

        [StringLength(14)]
        public string phone { get; set; }

        [StringLength(30)]
        public string phoneType { get; set; }

        [StringLength(3)]
        public string areaCode2 { get; set; }

        [StringLength(14)]
        public string phone2 { get; set; }

        [StringLength(30)]
        public string phone2Type { get; set; }

        [StringLength(3)]
        public string faxareaCode { get; set; }

        [StringLength(14)]
        public string fax { get; set; }

        public DateTime? created { get; set; }

        [StringLength(256)]
        public string createdBy { get; set; }

        public decimal latitude { get; set; }

        public decimal longitude { get; set; }

        [StringLength(500)]
        public string market { get; set; }

        public string storeMapXML { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(1000)]
        public string webUrl { get; set; }

        public bool? isCurrent { get; set; }

        public bool? isUpdated { get; set; }

        public bool? hasSubs { get; set; }

        public bool? hasDelivery { get; set; }

        public bool? newConstruction { get; set; }

        public DateTime? openDate { get; set; }

        [StringLength(40)]
        public string storeType { get; set; }

        [StringLength(100)]
        public string valueGroup { get; set; }

        public bool? corporateFlag { get; set; }

        [StringLength(20)]
        public string brandNameId { get; set; }

        [StringLength(200)]
        public string brandName { get; set; }

        [StringLength(3000)]
        public string bio { get; set; }

        [StringLength(5)]
        public string dpvReturnCode { get; set; }

        [StringLength(5)]
        public string code1ReturnCode { get; set; }

        [StringLength(20)]
        public string houseNumber { get; set; }

        [StringLength(10)]
        public string leadingDir { get; set; }

        [StringLength(50)]
        public string streetName { get; set; }

        [StringLength(20)]
        public string streetSuffix { get; set; }

        [StringLength(10)]
        public string trailingDir { get; set; }

        [StringLength(10)]
        public string dpvFalsePositiveFlag { get; set; }

        [StringLength(10)]
        public string dpvCmraFlag { get; set; }

        [StringLength(50)]
        public string addressKey { get; set; }

        [StringLength(5)]
        public string scf { get; set; }

        [StringLength(5)]
        public string bmc { get; set; }
    }
}
