namespace TdEfLib.orderingEf {
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblGenesisOrderStore {
        [Key]
        public int pk { get; set; }

        public int? orderPk { get; set; }

        public int? storePk { get; set; }

        public int? genesisInventoryPk { get; set; }

        public int? shippingType { get; set; }

        [StringLength(50)]
        public string attention { get; set; }

        [StringLength(50)]
        public string fedExLabelNote { get; set; }

        public int? unitSize { get; set; }

        public int? units { get; set; }

        [StringLength(50)]
        public string trackingNumber { get; set; }

        [StringLength(150)]
        public string shippingNotes { get; set; }

        public DateTime? shippedDate { get; set; }

        [StringLength(256)]
        public string shippedBy { get; set; }

        public bool? isShipped { get; set; }

        [JsonIgnore]
        public DateTime? created { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string createdby { get; set; }

        [JsonIgnore]
        public DateTime? modified { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string modifiedBy { get; set; }

        [JsonIgnore]
        public DateTime? deleted { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string deletedBy { get; set; }

        [JsonIgnore]
        public DateTime? submitted { get; set; }

        [JsonIgnore]
        [StringLength(256)]
        public string submittedBy { get; set; }

        public bool? isSubmitted{ get; set; }

        public bool? isCurrent { get; set; }

        [JsonIgnore]
        public virtual tblGenesisInventory tblGenesisInventory { get; set; }

        [JsonIgnore]
        public virtual tblOrder tblOrder { get; set; }

        [JsonIgnore]
        public virtual tblStoreProfile tblStoreProfile { get; set; }
    }
}
