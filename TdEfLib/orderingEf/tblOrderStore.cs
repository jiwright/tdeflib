namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrderStore
    {
        [Key]
        public int pk { get; set; }

        public int? orderPk { get; set; }

        public int? storePk { get; set; }

        public int? userPk { get; set; }

        public string orderStoreXML { get; set; }

        [StringLength(99)]
        public string selectedPostcard { get; set; }

        public DateTime? created { get; set; }

        [StringLength(256)]
        public string createdBy { get; set; }

        public DateTime? updated { get; set; }

        [StringLength(256)]
        public string updatedBy { get; set; }

        public virtual tblOrder tblOrder { get; set; }

        public virtual tblStoreProfile tblStoreProfile { get; set; }
    }
}
