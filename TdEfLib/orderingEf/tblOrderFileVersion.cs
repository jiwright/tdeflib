namespace TdEfLib.orderingEf
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrderFileVersion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrderFileVersion()
        {
            tblOrderFileVersionApprovals = new HashSet<tblOrderFileVersionApproval>();
        }
        [Key]
        public int fileVersionPk { get; set; }

        [JsonIgnore]
        public int FilePk { get; set; }

        [StringLength(250)]
        public string origFileName { get; set; }

        [JsonIgnore]
        [StringLength(250)]
        public string internalFileName { get; set; }

        [JsonIgnore]
        public decimal? fileSize { get; set; }

        [StringLength(200)]
        public string uuid { get; set; }

        public int? overallStatus { get; set; }

        [JsonIgnore]
        public DateTime? createdDate { get; set; }

        [JsonIgnore]
        [StringLength(100)]
        public string createdBy { get; set; }

        public DateTime? lastUpdateDate { get; set; }

        [StringLength(100)]
        public string lastUpdatedEmail { get; set; }

        [JsonIgnore]
        public bool? isCurrent { get; set; }

        [JsonIgnore]
        public virtual tblOrderFile tblOrderFile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<tblOrderFileVersionApproval> tblOrderFileVersionApprovals { get; set; }
    }
}
