namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblFacility
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int facilityId { get; set; }

        [StringLength(50)]
        public string facilityName { get; set; }

        [StringLength(100)]
        public string address1 { get; set; }

        [StringLength(100)]
        public string address2 { get; set; }

        [StringLength(50)]
        public string city { get; set; }

        [StringLength(5)]
        public string state { get; set; }

        [StringLength(10)]
        public string zip { get; set; }

        [StringLength(15)]
        public string phone { get; set; }

        [StringLength(15)]
        public string fax { get; set; }
    }
}
