namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblDefinition
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string tableName { get; set; }

        [StringLength(30)]
        public string columnName { get; set; }

        public int? fieldValue { get; set; }

        [StringLength(30)]
        public string fieldValueType { get; set; }

        [StringLength(50)]
        public string category { get; set; }

        [StringLength(255)]
        public string description { get; set; }

        public int? sortOrder { get; set; }

        public DateTime? created { get; set; }
    }
}
