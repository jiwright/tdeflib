namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblCustomer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCustomer()
        {
            tblStoreProfiles = new HashSet<tblStoreProfile>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int customerId { get; set; }

        [StringLength(50)]
        public string customerName { get; set; }

        [StringLength(14)]
        public string CustomerContact { get; set; }

        [StringLength(50)]
        public string customerAddress1 { get; set; }

        [StringLength(50)]
        public string customerAddress2 { get; set; }

        [StringLength(30)]
        public string customerCity { get; set; }

        [StringLength(5)]
        public string customerZip { get; set; }

        public DateTime? created { get; set; }

        [StringLength(100)]
        public string createdBy { get; set; }

        public DateTime? updated { get; set; }

        [StringLength(100)]
        public string updatedBy { get; set; }

        public int? parentCustomerId { get; set; }

        public bool? isDeleted { get; set; }

        [StringLength(100)]
        public string customerIndustry { get; set; }

        [StringLength(2)]
        public string customerState { get; set; }

        public int? monthlyEmailQuota { get; set; }

        [StringLength(100)]
        public string supportMail { get; set; }

        [StringLength(25)]
        public string suiteNumber { get; set; }

        public bool? isTrialCreditsAvailable { get; set; }

        public int? subAccountId { get; set; }

        [StringLength(100)]
        public string webUrl { get; set; }

        [StringLength(35)]
        public string firstName { get; set; }

        [StringLength(35)]
        public string lastName { get; set; }

        public int? sampleListId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblStoreProfile> tblStoreProfiles { get; set; }
    }
}
