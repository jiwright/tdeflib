namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderStoreApprovalHistory")]
    public partial class tblOrderStoreApprovalHistory
    {
        [Key]
        public int pk { get; set; }

        public int? customerId { get; set; }

        public int orderPk { get; set; }

        public int? storePk { get; set; }

        public int? approvalType { get; set; }

        public int? approvalStatus { get; set; }

        [StringLength(100)]
        public string userEmail { get; set; }

        public DateTime? created { get; set; }

        [StringLength(256)]
        public string createdBy { get; set; }

        [Column(TypeName = "xml")]
        public string orderApprovalXML { get; set; }

        public string notes { get; set; }
    }
}
