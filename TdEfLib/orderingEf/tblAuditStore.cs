namespace TdEfLib.orderingEf {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblAuditStore {
        [Key]
        [Column(Order = 0)]
        public int pk { get; set; }

        [Key]
        [Column(Order = 1)]
        public string storeId { get; set; }

        public float? radius { get; set; }

        public int? targeted { get; set; }

        public int? targetedAsk { get; set; }

        public int? actualCount { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime created { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool isCurrent { get; set; }

        public int? orderJobs_orderPk { get; set; }
    }
}
