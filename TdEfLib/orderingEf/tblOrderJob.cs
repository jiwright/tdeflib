namespace TdEfLib.orderingEf
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrderJob
    {
        public int? orderPk { get; set; }

        public int? facilityId { get; set; }

        public decimal? jobNumber { get; set; }

        public int? customerId { get; set; }

        [Key]
        public int pk { get; set; }
        [JsonIgnore]
        public virtual tblOrder tblOrder { get; set; }
    }
}
