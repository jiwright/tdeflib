namespace TdEfLib.orderingEf {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblOrderApprover {
        [Key]
        public int pk { get; set; }

        public int orderPk { get; set; }

        public DateTime created { get; set; }

        public bool? isCurrent { get; set; }

        [Required]
        [StringLength(50)]
        public string name { get; set; }

        [Required]
        [StringLength(50)]
        public string value { get; set; }

        [Required]
        [StringLength(50)]
        public string type { get; set; }

        [StringLength(50)]
        public string proofUserType { get; set; }

        public virtual tblOrder tblOrder { get; set; }
    }
}
