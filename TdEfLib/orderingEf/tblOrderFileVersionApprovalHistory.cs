using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdEfLib.orderingEf {
    [Table("tblOrderFileVersionApprovalHistory")]
    public partial class tblOrderFileVersionApprovalHistory {
        [Key]
        public int pk { get; set; }

        public int? customerId { get; set; }

        public int orderPk { get; set; }

        public int? fileVersionPk { get; set; }

        public int? proofTypeId { get; set; }

        public int? approvalType { get; set; }

        public int? approvalStatus { get; set; }

        [StringLength(100)]
        public string userEmail { get; set; }

        public DateTime? created { get; set; }

        [StringLength(256)]
        public string createdBy { get; set; }

        [Column(TypeName = "xml")]
        public string orderApprovalXML { get; set; }

        public string notes { get; set; }

        [StringLength(200)]
        public string approvalEmail { get; set; }

        [StringLength(200)]
        public string approvalTitle { get; set; }

        public int? approvalLevel { get; set; }

        public int? filePk { get; set; }
    }
}
