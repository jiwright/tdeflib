namespace TdEfLib.orderingEf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblBrandCustomer
    {
        [Key]
        public int pk { get; set; }

        [StringLength(30)]
        public string brandCustomerId { get; set; }

        [StringLength(500)]
        public string brandName { get; set; }

        [StringLength(30)]
        public string franchiseCustomerId { get; set; }
    }
}
